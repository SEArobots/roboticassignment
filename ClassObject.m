%for all objects in the environment

classdef ClassObject
    properties
        plyFile
        pos
        verts
        vertexCount
        mesh
    end
    methods
        function self = Draw(self) %function that loads the .ply file
            %load ply file
            [f,v,data] = plyread(self.plyFile,'tri');
            % Get vertex count
            self.vertexCount = size(v,1);
            % Move center point to origin
            midPoint = sum(v)/self.vertexCount;
            self.verts = v - repmat(midPoint,self.vertexCount,1);
            % Scale the colours to be 0-to-1 (they are originally 0-to-255
            vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;
            % Then plot the trisurf
            self.mesh = trisurf(f,self.verts(:,1),self.verts(:,2), self.verts(:,3) ...
                ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');
            %transl points
            updatedPoints = [self.pos * [self.verts,ones(self.vertexCount,1)]']'; 
            self.mesh.Vertices = updatedPoints(:,1:3);
            drawnow()
        end
        function self = Move(self,dpos) %function that plots the object
            self.pos=dpos; 
            updatedPoints = [self.pos * [self.verts,ones(self.vertexCount,1)]']'; 
            self.mesh.Vertices = updatedPoints(:,1:3);
            drawnow()   
        end
        
    end
end