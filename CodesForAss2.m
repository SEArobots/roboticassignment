
clear all
clc
clf

%% setting DH parameters


L1 = Link('d',0.05,'a',0,'alpha',-pi/2,'offset',0,'qlim',[deg2rad(-135),deg2rad(135)]) %joint 1 

L2 = Link('d',0,'a',0.135,'alpha',0,'offset',-pi/2,'qlim',[deg2rad(5),deg2rad(80)]) %joint 2

L3 = Link('d',0,'a',0.160,'alpha',0,'offset',0,'qlim',[deg2rad(15),deg2rad(170)]) %joint 3

L4 = Link('d',0,'a',0.05,'alpha',-pi/2, 'offset',0, 'qlim',[-pi/2,pi/2]) %joint 4

L5 = Link('d',0.05,'a',0,'alpha',0,'offset',0 ,'qlim',[deg2rad(-85),deg2rad(85)]) %joint 5

myRobot1 = SerialLink([L1 L2 L3 L4 L5], 'name', 'Dobot');
for linkIndex = 0:myRobot1.n      %% arm 1
    [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['Link',num2str(linkIndex),'.ply'],'tri'); %%use plyread to get the 3d models
    myRobot1.faces{linkIndex+1} = faceData;   %assign corresponding faces and vertices 
    myRobot1.points{linkIndex+1} = vertexData;
end



workspace = [-0.7 0.7 -0.7 0.7 -0.7 1.5];


% qr=DobotIkine(myRobot1,0.15,0.15,0);
qr=DobotIkine(myRobot1,0.18,0.18,0.15);


myRobot1.plot3d(qr,'workspace',workspace);
hold on

% 
table = ClassObject;
table.plyFile='table.ply';
table.pos=transl(0,0,-0.2)*trotz(90,'deg');
table = table.Draw(); %get the model to appear in workspace


% curtain = ClassObject;
% curtain.plyFile='laser curtain.ply';
% curtain.pos=transl(0,0,0)*trotz(90,'deg');
% curtain = curtain.Draw(); %get the model to appear in workspace

% 
% estop = ClassObject;
% estop.plyFile='estop.ply';
% estop.pos=transl(0,0.4,0.7);
% estop = estop.Draw(); %get the model to appear in workspace

% %create top housing, assign .ply model and initial position 
% joystick = ClassObject;
% joystick.plyFile='joystick.ply';
% joystick.pos=transl(0,0,0.7);
% joystick = joystick.Draw(); %get the model to appear in workspace

%create top housing, assign .ply model and initial position 
% block = ClassObject;
% block.plyFile='block.ply';
% block.pos=transl(0,0.2,0);
% block = block.Draw(); %get the model to appear in workspace
% 
% block2 = ClassObject;
% block2.plyFile='block.ply';
% block2.pos=transl(0.3,0.1,0);
% block2 = block2.Draw(); %get the model to appear in workspace


%myRobot1.teach
% RobotMove(myRobot1,0,0.2,0.03,block,false);
% RobotMove(myRobot1,0,0.2,0.1,block,true);
% RobotMove(myRobot1,0,0.1,0.03,block2,false);
% RobotMove(myRobot1,0,0.26,0.1,block2,true);

camlight

centerpnt1 = [0.2,0.2,0];
%{ 
for moving stuff


side = 0.05;
plotOptions.plotFaces = true;
[vertex,faces,faceNormals,h1] = RectangularPrism(centerpnt1-side/2, centerpnt1+side/2,plotOptions);

centerpnt2 = [0,0.2,0];
side = 0.05;
plotOptions.plotFaces = true;
[vertex,faces,faceNormals,h2] = RectangularPrism(centerpnt2-side/2, centerpnt2+side/2,plotOptions);

centerpnt3 = [-0.2,0.2,0];
side = 0.05;
plotOptions.plotFaces = true;
[vertex,faces,faceNormals,h3] = RectangularPrism(centerpnt3-side/2, centerpnt3+side/2,plotOptions);

RobotMove(myRobot1,0.2,0.2,0,centerpnt1,false,false);
RobotMove(myRobot1,0,0.2,0.1,centerpnt2,true,true,h1);
RobotMove(myRobot1,0,0.18,0.05,centerpnt2,true,false,h1);

RobotMove(myRobot1,-0.2,0.2,0,centerpnt3,false,false);
RobotMove(myRobot1,0,0.2,0.2,centerpnt2,true,true,h3);
RobotMove(myRobot1,0,0.18,0.1,centerpnt2,true,false,h3);
%}


% while(pi)
%     GUI()
%     pause();
%     a = GUI();
%     RobotMove(myRobot1,a(1,1),a(2,1),a(3,1),centerpnt1,false,false);
% end


RobotMoveJoystick(myRobot1)