%% LinePlaneIntersection
% Given a plane (normal and point) and two points that make up another line, get the intersection
% Check == 0 if there is no intersection
% Check == 1 if there is a line plane intersection between the two points
% Check == 2 if the segment lies in the plane (always intersecting)
% Check == 3 if there is intersection point which lies outside line segment
function [intersection, topPlane] = LinePlaneIntersection3 (myRobot1,move_matrix,centerpnt, mode )
intersection =0;
topPlane=0;
% centerpnt = [-8,1,0.1];
side = 0.05;
plotOptions.plotFaces = true;
[vertex,faces,faceNormals,k] = RectangularPrism(centerpnt-side/2,centerpnt+side/2,plotOptions);
%     for i = 1:1:size(move_matrix,1)
    tr = myRobot1.fkine(move_matrix);
        if mode 
        startP=tr(1:3,4)'+ (0.06)*tr(1:3,3)'; %of the end effector
        endP = tr(1:3,4)'+ (0.066)*tr(1:3,3)';%from the end effector
%          plot3(startP(1),startP(2),startP(3),'r*')
%         plot3(endP(1),endP(2),endP(3),'g*')
%         hold on
        else
        startP=tr(1:3,4)'; %of the end effector
        endP = tr(1:3,4)' + (0.04)* tr(1:3,3)';%from the end effector
        end
       
 
        for faceIndex = 1:size(faces,1)
            vertOnPlane = vertex(faces(faceIndex,1)',:);
            [intersectP,check] = LinePlaneIntersection(faceNormals(faceIndex,:),vertOnPlane,startP,endP); 
            if check == 1 && IsIntersectionPointInsideTriangle(intersectP,vertex(faces(faceIndex,:)',:))
                display('Intersection');
                intersection =1;
                if check ==1 && LinePlaneIntersection2(faceNormals(faceIndex,:), vertOnPlane)
                    topPlane=1;
                else
                    topPlane=0;
                end
                break
                
            end
        end    

%         if check ==1
%             break
%         end
%     myRobot1.plot(move_matrix(i,:))
%     end
   delete(k);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%
function [intersectionPoint,check] = LinePlaneIntersection(planeNormal,pointOnPlane,point1OnLine,point2OnLine)
LinePlaneIntersection2(planeNormal,pointOnPlane);
intersectionPoint = [0 0 0];
u = point2OnLine - point1OnLine;
w = point1OnLine - pointOnPlane;
D = dot(planeNormal,u);
N = -dot(planeNormal,w);
check = 0; %#ok<NASGU>
if abs(D) < 10^-7        % The segment is parallel to plane
    if N == 0           % The segment lies in plane
        check = 2;
        return
    else
        check = 0;       %no intersection
        return
    end
end

%compute the intersection parameter
sI = N / D;
intersectionPoint = point1OnLine + sI.*u;

if (sI < 0 || sI > 1)
    check= 3;          %The intersection point  lies outside the segment, so there is no intersection
else
    check=1;
    
end
end

function [result] = LinePlaneIntersection2(planeNormal,pointOnPlane)
a= [1,1,0];
if dot(planeNormal,a) == 0 && pointOnPlane(3)>-0.5
    result = true;
else
    result = false;
end
return;
end

function result = IsIntersectionPointInsideTriangle(intersectP,triangleVerts)

u = triangleVerts(2,:) - triangleVerts(1,:);
v = triangleVerts(3,:) - triangleVerts(1,:);

uu = dot(u,u);
uv = dot(u,v);
vv = dot(v,v);

w = intersectP - triangleVerts(1,:);
wu = dot(w,u);
wv = dot(w,v);

D = uv * uv - uu * vv;

% Get and test parametric coords (s and t)
s = (uv * wv - vv * wu) / D;
if (s < 0.0 || s > 1.0)        % intersectP is outside Triangle
    result = 0;
    return;
end

t = (uv * wu - uu * wv) / D;
if (t < 0.0 || (s + t) > 1.0)  % intersectP is outside Triangle
    result = 0;
    return;
end

result = 1;                      % intersectP is in Triangle
end



function objectCollision(center1,center2,side)
v = center1-center2;
dist = sprt(v(1)^2+ v(2)^2+ v(3)^2);
minimum = 2*cos(deg2rad(45))*side +0.05;
if dist <= minimum
    display('collision between objects can happen');
end
end

function scan_surrounding

end
%bonus
% scan up to 3m in the invironment
% find all intersection
% if abs(x1-x2) || abs(y1-y2) < 0.5
%     m(i,:)=[x2,y2]
%     x1 = x2
%     y1 = y2
%     