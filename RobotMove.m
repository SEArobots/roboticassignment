function [centerpnt0]=RobotMove (robot,x_,y_,z_,check_cen,mode,on_the_move,varargin)

z_=z_+0.05;
k=0;
intersect=0;


    steps = 50;
    workspace = [-0.5 0.5 -0.5 0.5 -0.5 0.5];
    currentPos = [robot.getpos];
    T1 = robot.fkine(currentPos);
    T1 = T1(1:3,4);
    desiredPos = [DobotIkine(robot,x_,y_,z_)];
    T2 = robot.fkine(desiredPos);

    deltaT = 0.05;                                        % Discrete time step
    x = nan(3,steps);
    %x = zeros(robot.n,steps);
    s = lspb(0,1,steps);                                 % Create interpolation scalar
    for i = 1:steps
        x(:,i) = T1*(1-s(i)) + s(i)*[x_ y_ z_]';

    end
   plot3(x(1,:),x(2,:),x(3,:),'k.','LineWidth',1);

    qMatrix = nan(steps,robot.n);

    % 3.9
    qMatrix(1,:) = currentPos;

    % 3.10
    for i = 1:steps-1
        xdot = (x(:,i+1) - x(:,i))/deltaT;                             % Calculate velocity at discrete time step
        J = robot.jacob0(qMatrix(i,:));            % Get the Jacobian at the current state
        J = J(1:3,1:3);                           
        qdot = inv(J)*xdot;                             % Solve velocitities via RMRC
        qMatrix(i+1,:) =  qMatrix(i,:) + deltaT*[qdot' 0 0];            

    end
    C=col_ver2;
    rem=0;
    human =0;
    axis([-2 2 -2 2 -2 2])
    for i = 1:steps
        
        [a,b] = STOP();
        if b ~= rem
            C.delete;
            rem=b;
            human = C.collision(b);

        end
        
        while(a||human)
            [a,b] = STOP();
            human = C.collision(b);
        end
        


        qMatrix(i,4) = pi/2 - qMatrix(i,3) - qMatrix(i,2);
        robot.animate(qMatrix(i,:));
        
        T=robot.fkine(qMatrix(i,:))*troty(pi); %get the robot actual end effector pos
        % plot3(T(1,4),T(2,4),T(3,4),'g*')
         if nargin>3
       [intersect,top_plane]= LinePlaneIntersection3(robot,qMatrix(i,:),check_cen,mode );
       
       if intersect ==1 && top_plane ==1
           display('intersection with top plane');
           break
       else
           if intersect ==1
           display('intersection with something on the way');
           break
           end
       end
            for order=1:length(varargin)
%                 varargin{i}.Move(T); %move attached objects to robot end effector
                
                centerpnt0 = T(1:3,4)' + [0.00,0.01,-0.055];  
%                +[0.0101,0.0134,0.0276*-1]
                delete(varargin{1});
                side = 0.05;
                plotOptions.plotFaces = true;
                [vertex,faces,faceNormals,varargin{1}] = RectangularPrism(centerpnt0-side/2, centerpnt0+side/2,plotOptions);
                if i ==50 && on_the_move
                    delete(varargin{1})
                end
            end
%              centerpnt = T(1:3,4)';
%              clear = dist(centerpnt,centerpnt2,0.053);
%              if clear ==false
%                                   display('2 objects are too close, collision is likely')
%                                     k=1;
%              break
%              end
   
    end




end
