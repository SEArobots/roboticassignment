function RobotMoveJoystick (robot)
steps = 50;
workspace = [-0.5 0.5 -0.5 0.5 -0.5 0.5];
currentPos = [robot.getpos];
T1 = robot.fkine(currentPos);
T1 = T1(1:3,4);
id = 1;
joy = vrjoystick(id);
caps(joy) % display joystick information
steps = 1;
qMatrix = nan(10000,robot.n);
qMatrix(1,:) = currentPos;
deltaT = 0.05;     
% lamda = 0.1; %damping coefficient


while(pi>1)
    
    % read joystick
    [axes, buttons, povs] = read(joy);

    K1 = 0.3;
    vx = K1*axes(1);
    vy = K1*axes(2);
    vz = K1*axes(6);
    wx =0;
    wy =0;
    wz =0;


    if (abs(vx)<0.02 && abs(vy)<0.02 && abs(vz)<0.02 )
    qMatrix(steps+1,:) =  qMatrix(steps,:);
    qMatrix(steps,4) = pi/2 - qMatrix(steps,3) - qMatrix(steps,2);
    else
    xdot = [vx vy vz]'
    
    J = robot.jacob0(qMatrix(steps,:));            % Get the Jacobian at the current state
    J = J(1:3,1:3);                           % Take only first 2 rows
%     Jdamped = J + lamda*eye(3) %DLS
    qdot = inv(J)*xdot;                             % Solve velocitities via RMRC
%     qdot = inv(Jdamped)*xdot
    qMatrix(steps+1,:) =  qMatrix(steps,:) + deltaT*[qdot' 0 0];
    qMatrix(steps,4) = pi/2 - qMatrix(steps,3) - qMatrix(steps,2);
    end

    if qMatrix(steps,:) < robot.qlim(:,2)'
        if robot.qlim(:,1)' < qMatrix(steps,:)
            robot.animate(qMatrix(steps,:)); 
            steps = steps+1;
        else
            display('hit qlim 1');
            if 3 < steps
            qMatrix(steps,:) = qMatrix(steps - 3,:);
            end
        end
    else
        display('hit qlim 2');
        if 3 < steps
        qMatrix(steps,:) = qMatrix(steps - 3,:);
        end
    end
    
    
    % wait until loop time elapsed
display(steps)
end
