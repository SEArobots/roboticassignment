function col_ver2 (1)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
L1 = Link('d',0,'a',0.1,'alpha',0,'qlim',[-pi pi]);

laser1 = SerialLink([L1],'name','laser1');
laser1.base = transl(0,0,0);
q=[0];
scale = 0.5;
workspace = [-4 4 -4 4 -1 2];  
laser1.plot(q,'workspace',workspace,'scale',scale);
hold on

point1 = [2,2,0.5];
point2 = [-2,2,0.5];
point3 = [2,-2,0.5];
point4 = [-2,-2,0.5];

line1_h = plot3([point1(1),point2(1)],[point1(2),point2(2)],[point1(3),point2(3)],'r');
hold on
line1_h = plot3([point2(1),point4(1)],[point2(2),point4(2)],[point2(3),point4(3)],'r');
% hold on
line1_h = plot3([point4(1),point3(1)],[point4(2),point3(2)],[point4(3),point3(3)],'r');
% hold on
line1_h = plot3([point1(1),point3(1)],[point1(2),point3(2)],[point1(3),point3(3)],'r');
% hold on
hold off
centerpnt = [-8,1,0.1];
side = 1.5;
plotOptions.plotFaces = true;
[vertex,faces,faceNormals,h] = RectangularPrism(centerpnt-side/2, centerpnt+side/2,plotOptions);
[vertex1,faces1,faceNormals1,h1] = RectangularPrism(centerpnt-side/2, centerpnt+side/2,plotOptions);
axis equal;
%  delete(h);
x=-4;
human = 0;

while human ==0
    delete(h);

    x=x+0.2;
    centerpnt = [x,1,0.1];
    side = 1.5;
    plotOptions.plotFaces = true;
    [vertex,faces,faceNormals,h] = RectangularPrism(centerpnt-side/2, centerpnt+side/2,plotOptions);
    
    drawnow();
   
    pause(1);
    
    for faceIndex = 1:size(faces,1)
        vertOnPlane = vertex(faces(faceIndex,1)',:);
        [intersectP1,check1] = LinePlaneIntersection(faceNormals(faceIndex,:),vertOnPlane,point1,point2);
        [intersectP2,check2] = LinePlaneIntersection(faceNormals(faceIndex,:),vertOnPlane,point2,point4);
        [intersectP3,check3] = LinePlaneIntersection(faceNormals(faceIndex,:),vertOnPlane,point4,point3);
        [intersectP4,check4] = LinePlaneIntersection(faceNormals(faceIndex,:),vertOnPlane,point3,point1);
%         
        if (check1 == 1 && IsIntersectionPointInsideTriangle(intersectP1,vertex(faces(faceIndex,:)',:)))...
            ||(check2 == 1 && IsIntersectionPointInsideTriangle(intersectP2,vertex(faces(faceIndex,:)',:)))...
            ||(check2 == 1 && IsIntersectionPointInsideTriangle(intersectP3,vertex(faces(faceIndex,:)',:)))...
            ||(check2 == 1 && IsIntersectionPointInsideTriangle(intersectP4,vertex(faces(faceIndex,:)',:)))
          
            human =1;
        end
    end
    if human ==1
         display('Object detected in workspace. Shut down operation.');
         clf;
         imshow('alert.png','Border','tight')
    end
end
end

