classdef col_ver2 < handle
    properties
        handle
    end
    
    
    methods
        function human = collision(self,x_)

hold on

point1 = [0.5,0.5,0.5];
point2 = [-0.5,0.5,0.5];
point3 = [0.5,-0.5,0.5];
point4 = [-0.5,-0.5,0.5];

line1_h = plot3([point1(1),point2(1)],[point1(2),point2(2)],[point1(3),point2(3)],'r');
hold on
line1_h = plot3([point2(1),point4(1)],[point2(2),point4(2)],[point2(3),point4(3)],'r');
% hold on
line1_h = plot3([point4(1),point3(1)],[point4(2),point3(2)],[point4(3),point3(3)],'r');
% hold on
line1_h = plot3([point1(1),point3(1)],[point1(2),point3(2)],[point1(3),point3(3)],'r');
hold on
axis equal;
x=-1;
human = 0;

    

    x=x+x_;
    centerpnt = [x,0.5,0.5];
    side = 0.5;
    plotOptions.plotFaces = true;
    [vertex,faces,faceNormals,self.handle] = RectangularPrism(centerpnt-side/2, centerpnt+side/2,plotOptions);
  
     axis([-2 2 -2 2 -2 2])
   
    
    
    for faceIndex = 1:size(faces,1)
        vertOnPlane = vertex(faces(faceIndex,1)',:);
        [intersectP1,check1] = LinePlaneIntersection(faceNormals(faceIndex,:),vertOnPlane,point1,point2);
        [intersectP2,check2] = LinePlaneIntersection(faceNormals(faceIndex,:),vertOnPlane,point2,point4);
        [intersectP3,check3] = LinePlaneIntersection(faceNormals(faceIndex,:),vertOnPlane,point4,point3);
        [intersectP4,check4] = LinePlaneIntersection(faceNormals(faceIndex,:),vertOnPlane,point3,point1);
%         
        if (check1 == 1 && IsIntersectionPointInsideTriangle(intersectP1,vertex(faces(faceIndex,:)',:)))...
            ||(check2 == 1 && IsIntersectionPointInsideTriangle(intersectP2,vertex(faces(faceIndex,:)',:)))...
            ||(check2 == 1 && IsIntersectionPointInsideTriangle(intersectP3,vertex(faces(faceIndex,:)',:)))...
            ||(check2 == 1 && IsIntersectionPointInsideTriangle(intersectP4,vertex(faces(faceIndex,:)',:)))
          
            human =1;
        end
    end
    if human ==1
         display('Object detected in workspace. Shut down operation.');
         %imshow('alert.png','Border','tight');
         
    end
    
    
        end
        
        function self = delete(self)
            delete(self.handle);
           
        end
    end
    
%end
end

